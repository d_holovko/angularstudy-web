import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BoardClient, BoardEntity, ListEntity, CardEntity } from '../nswag';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
  providers: [BoardClient]
})
export class BoardComponent implements OnInit {
  board: BoardEntity;
  listModel: ListEntity = new ListEntity();
  cardModel: CardEntity = new CardEntity();

  constructor(
    private route: ActivatedRoute,
    private client: BoardClient,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.getBoardData();
  }

  getBoardData() {
    const id = this.route.snapshot.paramMap.get('id');
    this.client.getBoard(id).subscribe(
      response => {
        this.board = response;
        this.listModel.boardId = this.board.id;
      },
      error => console.log(error));
  }

  addList() {
    this.client.postList(this.listModel).subscribe(
      response => {
        console.log(response);
        this.modalService.dismissAll();
        this.getBoardData();
      },
      error => console.log(error)
    );
    this.listModel.name = null;
  }

  addCard() {
    this.client.postCard(this.cardModel).subscribe(
      response => {
        console.log(response);
        this.modalService.dismissAll();
        this.getBoardData();
      },
      error => console.log(error)
    );
    this.cardModel.text = null;
  }

  openAddListModal(content) {
    this.modalService.open(content, { centered: true });
  }

  openAddCardModal(content, listId) {
    this.modalService.open(content, { centered: true });
    this.cardModel.listId = listId;
  }
}
