import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { BoardClient, BoardEntity } from '../nswag';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css'],
  providers: [BoardClient]
})
export class BoardsComponent implements OnInit {
  boards: BoardEntity[];
  model: BoardEntity = new BoardEntity();

  constructor(private client: BoardClient, private modalService: NgbModal) { }

  ngOnInit() {
    this.getBoards();
  }

  getBoards() {
    this.client.getBoards().subscribe(response => this.boards = response, error => console.log(error));
  }

  addBoard() {
    this.client.postBoard(this.model).subscribe(
      response => console.log(response),
      error => console.log(error),
      () => {
        this.modalService.dismissAll();
        this.getBoards();
      });
    this.model.name = null;
  }

  openModal(content) {
    this.modalService.open(content, { centered: true });
  }
}
