import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { BoardsComponent } from './boards/boards.component';

const appRoutes: Routes = [
   { path: '', component: HomeComponent},
   { path: 'boards', component: BoardsComponent },
   { path: 'board/:id', component: BoardComponent},
   { path: '**', redirectTo: '/'}
];

@NgModule({
   declarations: [
      AppComponent,
      BoardComponent,
      NavComponent,
      HomeComponent,
      BoardsComponent
   ],
   imports: [
      BrowserModule,
      RouterModule.forRoot(appRoutes),
      HttpClientModule,
      FormsModule,
      NgbModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
